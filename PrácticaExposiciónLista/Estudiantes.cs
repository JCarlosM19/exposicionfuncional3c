﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrácticaExposiciónLista
{
    public class Estudiantes
    {
        //Asignación de atributos
        public int ID { get; set; }
        public string Nombre { get; set; }
        public int Nota { get; set; }
        public string Paralelo { get; set; }
    }
}
