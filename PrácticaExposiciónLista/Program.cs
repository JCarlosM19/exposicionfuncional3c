﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PrácticaExposiciónLista
{
    class Program
    {
        static void Main(string[] args)
        {
            //Creación de una lista
            List<Estudiantes> Lista = new List<Estudiantes>();

            //Creación de los estudiantes
            Estudiantes Estudiante1 = new Estudiantes();
            Estudiante1.ID = 1;
            Estudiante1.Nombre = "Palacios Cedeño Luis";
            Estudiante1.Nota = 7;
            Estudiante1.Paralelo = "C";

            Estudiantes Estudiante2 = new Estudiantes();
            Estudiante2.ID = 2;
            Estudiante2.Nombre = "Lucas Garcia Miguel";
            Estudiante2.Nota = 10;
            Estudiante2.Paralelo = "A";

            Estudiantes Estudiante3 = new Estudiantes();
            Estudiante3.ID = 3;
            Estudiante3.Nombre = "Hernandez Alcivar Roberto";
            Estudiante3.Nota = 8;
            Estudiante3.Paralelo = "B";

            Estudiantes Estudiante4 = new Estudiantes();
            Estudiante4.ID = 4;
            Estudiante4.Nombre = "Palacios Alarcon David";
            Estudiante4.Nota = 9;
            Estudiante4.Paralelo = "C";

            Estudiantes Estudiante5 = new Estudiantes();
            Estudiante5.ID = 5;
            Estudiante5.Nombre = "Garces Gomez Hugo";
            Estudiante5.Nota = 6;
            Estudiante5.Paralelo = "A";

            //Adición de los estudiantes a la lista
            Lista.Add(Estudiante1);
            Lista.Add(Estudiante2);
            Lista.Add(Estudiante3);
            Lista.Add(Estudiante4);
            Lista.Add(Estudiante5);

            //Recorrido en una lista por medio del Foreach
            Console.WriteLine("***** Recorrido de una lista *****");
            foreach (Estudiantes estudiante in Lista)
            {
                Console.WriteLine("El ID es: " + estudiante.ID + " El nombre es: " + estudiante.Nombre + "| Su nota es: " + estudiante.Nota + "| Paralelo: " + estudiante.Paralelo);
            }

            //Búsqueda en una lista
            Console.WriteLine("\n***** Búsqueda en una lista *****");
            //Creación de una bandera
            bool buscar = false;
            Console.WriteLine("Ingrese el nombre del estudiante a buscar: ");
            string nombrebusqueda = Console.ReadLine();
            foreach (Estudiantes estudiante in Lista)
            {
                if (estudiante.Nombre == nombrebusqueda)
                {
                    buscar = true;
                    Console.WriteLine("\nEl ID es: " + estudiante.ID + " El nombre es: " + estudiante.Nombre + "| Su nota es: " + estudiante.Nota + "| Paralelo: " + estudiante.Paralelo);
                }
            }
            if (buscar == false)
            {
                Console.WriteLine("\nEl estudiante " + nombrebusqueda + " no está en la lista");
            }

            //Filtro en la lista por paralelo
            Console.WriteLine("\n***** Filtro de una lista por paralelo *****");
            Console.WriteLine("Ingrese el paralelo a filtrar: ");
            string paralelo = Console.ReadLine();
            foreach (Estudiantes estudiante in Lista.Where(estudiante => (estudiante.Paralelo == paralelo)))
            {
                Console.WriteLine("El nombre es: " + estudiante.Nombre + "| Paralelo: " + estudiante.Paralelo);
            }

            //Filtro en la lista por notas
            Console.WriteLine("\n***** Filtro de una lista por notas *****");
            Console.WriteLine("Ingrese el rango inferior de la nota: ");
            int notainf = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el rango superior de la nota: ");
            int notasup = int.Parse(Console.ReadLine());
            foreach (Estudiantes estudiante in Lista.Where(estudiante => (estudiante.Nota >= notainf && estudiante.Nota <= notasup)))
            {
                Console.WriteLine("El nombre es: " + estudiante.Nombre + "| Su nota es: " + estudiante.Nota + "| Paralelo: " + estudiante.Paralelo);
            }

            //Ordenamiento de una lista
            Console.WriteLine("\n***** Ordenamineto de la lista *****");
            List<Estudiantes> ListaNotas = Lista.OrderBy(estudiante => estudiante.Nombre).ToList();
            foreach (Estudiantes estudiante in ListaNotas)
            {
                Console.WriteLine("El ID es: " + estudiante.ID + " El nombre es: " + estudiante.Nombre + "| Su nota es: " + estudiante.Nota + "| Paralelo: " + estudiante.Paralelo);
            }

        }
    }
}
